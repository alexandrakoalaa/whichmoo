import React from 'react';
import { 
  Text,
  View,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Animated,
  Dimensions,
  SafeAreaView,
  PanResponder
} from 'react-native';

// import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import { getMovies } from '../store/actions/movie';

import  { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';

let SCREEN_WIDTH = Dimensions.get('window').width;
let SCREEN_HEIGHT = Dimensions.get('window').height;

// const addFilm = (film) => {
//   console.log('film', film);
//   return {
//   type: 'ADD_FILM',
//   payload: film,
// }}

// case 'ADD_FILM':
//   return {
//     allMovies: {
//       ...state.allMovies,
//       ...action.payload,
//     }

//   }

const mapStateToProps = (state) => {
  return {
    allMovies: state.movie.allMovies,
    bookedMovies: state.movie.bookedMovies
  }
}

const mapDispatchToProps = {
  getMovies
}

class MovieCards extends React.Component {
  constructor() {
    super();
    this.sPosition = new Animated.ValueXY();

    this.rotate = this.sPosition.x.interpolate({
      inputRange: [-SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2],
      outputRange: ['-10deg', '0deg', '10deg'],
      extrapolate: 'clamp'
    });

    this.likeOpacity = this.sPosition.x.interpolate({
      inputRange: [-SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2],
      outputRange: [0, 0, 1],
      extrapolate: 'clamp'
    });

    this.dislikeOpacity = this.sPosition.x.interpolate({
      inputRange: [-SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2],
      outputRange: [1, 0, 0],
      extrapolate: 'clamp'
    });

    this.nextCardOpacity = this.sPosition.x.interpolate({
      inputRange: [-SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2],
      outputRange: [1, 0, 1],
      extrapolate: 'clamp'
    });

    this.nextCardScale = this.sPosition.x.interpolate({
      inputRange: [-SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2],
      outputRange: [1, 0.8, 1],
      extrapolate: 'clamp'
    })

    this.state = {
      activeImage: null,
      activeTitle: null,
      activeOverview: null,
      currentIndex: 0,
      movies: []
    }

    this.rotateAndTranslate = {
      transform: [{
        rotate: this.rotate
      },
      ...this.sPosition.getTranslateTransform()
      ]
    }

  }
  UNSAFE_componentWillMount() {
    this.allImages = {};
    this.oldPosition = {};
    this.position = new Animated.ValueXY();
    this.dimensions = new Animated.ValueXY();
    this.animation = new Animated.Value(0);
    this.activeImageStyle = null;

    this.PanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (ev, gestureState) => true,
      onPanResponderMove: (ev, gestureState) => {
        this.sPosition.setValue({ x: gestureState.dx, y: gestureState.dy });
      },
      onPanResponderRelease: (ev, gestureState) => {
        if (gestureState.dx > 120) {
          Animated.spring(this.sPosition, {
            toValue: {x: SCREEN_WIDTH + 100, y: gestureState.dy}
          }).start(() => {
            this.props.allMovies[this.state.currentIndex].booked = true;
            this.props.bookedMovies.push(this.props.allMovies[this.state.currentIndex]);
            this.setState({ currentIndex: this.state.currentIndex + 1 }, () => {
              this.sPosition.setValue({ x: 0, y: 0 })
            })
          })
        } else if (gestureState.dx < -120) {
          Animated.spring(this.sPosition, {
            toValue: {x: -SCREEN_WIDTH - 100, y: gestureState.dy}
          }).start(() => {
            this.setState({ currentIndex: this.state.currentIndex + 1 }, () => {
              this.sPosition.setValue({ x: 0, y: 0 })
            })
          })
        } else {
          Animated.spring(this.sPosition, {
            toValue: { x: 0, y: 0 },
            friction: 4
          }).start();
        }
      }
    })
  }

  componentDidMount () {
    this.props.getMovies();
  }

  openImage = (index) => {
    this.allImages[index].measure((x, y, width, height, pageX, pageY) => {
      this.oldPosition.x = pageX;
      this.oldPosition.y = pageY;
      this.oldPosition.width = width;
      this.oldPosition.height = height;

      this.position.setValue({
        x: pageX,
        y: pageY
      });

      this.dimensions.setValue({
        x: width, 
        y: height
      });

      const {
        title,
        overview
       } = this.props.allMovies[index];

      this.setState({
        activeImage: `https://image.tmdb.org/t/p/w300${this.props.allMovies[index].poster_path}`,
        activeOverview: overview,
        activeTitle: title
      }, () => {
        this.viewImage.measure((dx, dy, dWidth, dHeight, dPageX, dPageY) => {
          Animated.parallel([
            Animated.timing(this.position.x, {
              toValue: dPageX,
              duration: 300
            }),
            Animated.timing(this.position.y, {
              toValue: dPageY,
              duration: 300
            }),
            Animated.timing(this.dimensions.x, {
              toValue: dWidth,
              duration: 300
            }),
            Animated.timing(this.dimensions.y, {
              toValue: dHeight,
              duration: 300
            }),
            Animated.timing(this.animation, {
              toValue: 1,
              duration: 300
            })
          ]).start();
        })
      })
    })
  }

  closeImage = () => {
    Animated.parallel([
      Animated.timing(this.position.x, {
        toValue: this.oldPosition.x,
        duration: 300
      }),
      Animated.timing(this.position.y, {
        toValue: this.oldPosition.y,
        duration: 300
      }),
      Animated.timing(this.dimensions.x, {
        toValue: this.oldPosition.width,
        duration: 300
      }),
      Animated.timing(this.dimensions.y, {
        toValue: this.oldPosition.height,
        duration: 300
      }),
      Animated.timing(this.animation, {
        toValue: 0,
        duration: 300
      })
    ]).start(() => {
      this.setState({
        activeImage: null
      })
    });
  }

  renderMovies = () => {
    return this.props.allMovies.map((movie, index) => {
      if (index < this.state.currentIndex) {
        return null;
      } else if (index == this.state.currentIndex) {
        return (
          <Animated.View
            {...this.PanResponder.panHandlers}
            style={[styles.animatedView, this.rotateAndTranslate]}
            key={movie.id}
          >
            <Animated.View 
              style={[{transform: [{rotate: '-30deg'}], opacity: this.likeOpacity, left: 40}, styles.swipeTextView]}
            >
              <Text style={[styles.swipeText, styles.like]}>LIKE</Text>
            </Animated.View>

            <Animated.View 
              style={[{transform: [{rotate: '30deg'}], opacity: this.dislikeOpacity, right: 40}, styles.swipeTextView]}
            >
              <Text style={[styles.swipeText, styles.dislike]}>NOPE</Text>
            </Animated.View>

            <Image
              ref={(movie) => (this.allImages[index] = movie)}
              source={{ uri: `https://image.tmdb.org/t/p/w300${movie.poster_path}` }}
              style={styles.image}
            />            

            <TouchableWithoutFeedback
              onPress={() => this.openImage(index)}
            >
              <Text style={{color: '#fff'}}>Description</Text>
            </TouchableWithoutFeedback>
          </Animated.View>
        )
      } else {
        return(
          <Animated.View
            style={[
              styles.animatedView,
              this.nextCardOpacity,
              {transform: [{scale: this.nextCardScale}]}
            ]}
            key={movie.id}
          >
            <Image
              ref={(movie) => (this.allImages[index] = movie)}
              source={{ uri: `https://image.tmdb.org/t/p/w300${movie.poster_path}` }}
              style={styles.image}
            />            

            <TouchableWithoutFeedback
              onPress={() => this.openImage(index)}
            >
            <Text style={{color: '#fff'}}>Description</Text>
          </TouchableWithoutFeedback>
        </Animated.View>
        );
      }
    }).reverse();
  }

  render() {
    const activeImageStyle = {
      width: this.dimensions.x,
      height: this.dimensions.y,
      left: this.position.x,
      top: this.position.y
    }

    const animatedContentY = this.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [-120, 0]
    });

    const animatedContentOpacity = this.animation.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 1]
    });

    const animatedContentStyle = {
      opacity: animatedContentOpacity,
      transform: [{
        translateY: animatedContentY
      }]
    };

    const animatedCrossOpacity = {
      opacity: this.animation
    };

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#1a133b'}}>
        <View style={{flex: 1}}>
          {this.renderMovies()}
        </View>
        <View 
          style={StyleSheet.absoluteFill}
          pointerEvents={this.state.activeImage ? 'auto' : 'none'}
        >
          <View 
            style={{flex: 2, zIndex: 1001}}
            ref={view => (this.viewImage = view)}
          >
            <Animated.Image
              source={this.state.activeImage
                ? {uri: this.state.activeImage}
                : null}
              style={[styles.openedImage, activeImageStyle]}
            >
            </Animated.Image>
            <TouchableWithoutFeedback onPress={() => {this.closeImage()}}>
              <Animated.View style={[styles.closeAnimView, animatedCrossOpacity]}>
                {/* <Text style={styles.close}>X</Text> */}
                <Ionicons name='ios-close-circle' style={styles.close} />
              </Animated.View>
            </TouchableWithoutFeedback>
          </View>
          <Animated.View style={[styles.description, animatedContentStyle]}>
            <Text style={styles.descriptionHeader}>
              {this.state.activeTitle}
            </Text>
            <Text style={styles.descriptionContent}>
              {this.state.activeOverview}
            </Text>
          </Animated.View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  animatedView: {
    height: SCREEN_HEIGHT - 150, 
    width: SCREEN_WIDTH,
    padding: 15,
    position: 'absolute'
  },
  image: {
    flex: 1,
    height: null,
    width: null,
    resizeMode: 'cover',
    borderRadius: 20
  },
  openedImage: {
    resizeMode: 'cover',
    top: 0,
    left: 0,
    width: null,
    height: null
  },
  description: {
    flex: 1,
    backgroundColor: '#1a133b',
    padding: 20,
    paddingTop: 50,
    zIndex: 1000,
  },
  descriptionHeader: {
    fontSize: 24,
    paddingBottom: 10,
    color: '#fff'
  },
  descriptionContent: {
    color: '#fff'
  },
  closeAnimView: {
    position: 'absolute',
    top: 30,
    right: 30
  },
  close: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff'
  },
  swipeTextView: {
    position: 'absolute',
    top: 50,
    zIndex: 1000
  },
  swipeText: {
    borderWidth: 1,
    fontSize: 32,
    fontWeight: '800',
    padding: 10
  },
  like: {
    color: 'green',
    borderColor: 'green'
  },
  dislike: {
    color: 'red',
    borderColor: 'red'
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(MovieCards);
