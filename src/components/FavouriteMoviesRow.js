import React from 'react';

import { 
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
  Dimensions
} from 'react-native';

import { removeMovie } from '../store/actions/movie';
import { useDispatch, useSelector } from 'react-redux';

const {width, height} = Dimensions.get('window');

export default function FavouriteMoviesRow(props) {
  const {
    movie,
    index
  } = props;

  const dispatch = useDispatch();

  const openMovie = () => {
    props.navigation.navigate('FavouriteMovie', { 
      movie: props.movie
    });
  }

  const removeHandler = () => {
    Alert.alert(
      'Удаление фильма',
      'Вы точно хотите удалить фильм?',
      [
        {text: 'Отменить'},
        {
          text: 'Удалить',
          onPress: () => {
            dispatch(removeMovie(movie.id))
          }
        }
      ]
    )
  }

  return(
    <TouchableOpacity 
      onPress={openMovie}
    >
      <View
        style={[
          {width: (width)/3},
          {height: (height)/4},
          {marginBottom: 10},
          index % 3 !== 0 ? {paddingLeft: 5} : {paddingLeft: 0}
        ]}>
        <Image 
          style={{flex: 1, resizeMode: 'cover'}}
          source={{ uri: `https://image.tmdb.org/t/p/w300${movie.poster_path}` }}
        />
        <Text
          style={{color: '#fff', fontSize: 16}}
          numberOfLines={1}
        >
          {movie.title}
        </Text>
        <TouchableWithoutFeedback onPress={removeHandler}>
          <Text style={styles.removeText}>Удалить</Text>
        </TouchableWithoutFeedback>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  image: {
    width: null,
    height: 200,
    resizeMode: 'contain',
    padding: 5,
    minWidth: 50
  },
  removeText: {
    color: 'grey'
  },
  info: {
    marginHorizontal: 40,
    marginVertical: 10,
    padding: 10,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 4
  }
});