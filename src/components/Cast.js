import React from 'react'
import {View, Text, Dimensions, StyleSheet, Image} from 'react-native';

import profile from '../images/profile.png';

const {width} = Dimensions.get('window');

export const Cast = ({cast}) => {
  const {name, profile_path, character, index} = cast;

  return (
    <View
      style={[
        {width: (width)/4},
        {marginBottom: 2},
        index % 5 !== 0 ? {paddingLeft: 15} : {paddingLeft: 0}
      ]}>
      <Image 
        style={styles.image}
        source={{ uri: `https://image.tmdb.org/t/p/w300${profile_path}`}}
      />
      <Text
      numberOfLines={1}
        style={styles.name}
      >
        {name}
      </Text>
      <Text style={styles.character} numberOfLines={1}>{character}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 70,
    height: 70,
    resizeMode: 'cover',
    borderRadius: 50
  },
  name: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center'
  },
  character: {
    color: '#fff',
    fontSize: 12
  }
});