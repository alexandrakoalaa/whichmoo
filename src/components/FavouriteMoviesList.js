import React, { useState, useEffect } from 'react';

import { useSelector } from 'react-redux';

import {
  Text,
  View,
  FlatList,
  TextInput,
  StyleSheet,
} from 'react-native';

import FavouriteMoviesRow from './FavouriteMoviesRow';

export const FavouriteMoviesList = (props) => {
  const [search, setSearch] = useState(null);
  const bookedMovies = useSelector(state => state.movie.bookedMovies);
  
  return(
    <View style={{
      flex: 1,
      backgroundColor: '#1a133b'
    }}>
      <View style={{
        marginTop: 50,
        alignItems: 'center'
      }}>
      </View>
      <View style={{paddingHorizontal: 20}}>
        <TextInput
          style={ styles.input }
          onChangeText={text => {
            setSearch(text);
          }}
          value={search}
          placeholder='Поиск'
        />
      </View>
      <View style={{flex: 1}}>
        <FlatList
          style={{flex: 1}}
          data={
            bookedMovies.filter(movie => {
              return !search || 
                movie.title.toLowerCase().indexOf(search.toLowerCase()) > -1
            })
          }
          renderItem = {({ item, index }) =>
            <FavouriteMoviesRow 
              movie={item}
              index={index}
              navigation={props.navigation}
            />
          }
          keyExtractor = {item => item.title}
          horizontal={false}
          numColumns={3}
        >
        </FlatList>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    marginBottom: 30,
    padding: 10,
    paddingHorizontal: 20,
    fontSize: 16,
    color: '#444',
    borderColor: '#ddd',
    backgroundColor: '#f5f5f5',
    borderRadius: 5
  }
});