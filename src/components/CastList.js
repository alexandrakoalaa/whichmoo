import React, {useState, useEffect} from 'react';
import {FlatList, View} from 'react-native';

import {Cast} from './Cast';

import {API_KEY} from '../api_key';

export const CastList = (props) => {
  const [cast, setCast] = useState([]);
  useEffect(() => {
    fetch(`https://api.themoviedb.org/3/movie/${props.movieId}/credits?api_key=${API_KEY}&language=ru-RU`)
      .then(res => res.json())
      .then(res => setCast(res.cast));
  }, [])
  return (
    <FlatList
      data={cast}
      renderItem = {({ item, index }) =>
        <Cast 
          cast={item}
          index={index}
        />
      }
      keyExtractor = {item => item.name}
      horizontal={true}
    >

    </FlatList>
  );
}