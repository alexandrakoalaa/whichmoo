import React from 'react';
import {API_KEY} from '../api_key';

import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import {WebView} from 'react-native-webview';

import {CastList} from './CastList';
import {Genres} from './Genres';

const HEADER_MAX_HEIGHT = 500;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 100 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default class FavouriteMovie extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      cast: null,
      trailerKey: null,
      showWebView: false
    };
  }

  componentDidMount() {
    const movie = this.props.navigation == null
      ? this.props.movie
      : this.props.navigation.getParam('movie');

    fetch(`https://api.themoviedb.org/3/movie/${movie.id}/videos?api_key=${API_KEY}&language=en-US`)
      .then(res => res.json())
      .then(res => this.setState({trailerKey: res.results[0].key}));
  }

  renderContent() {
    return (
      <WebView
        source={{uri: `https://www.youtube.com/watch?v=${this.state.trailerKey}`}}
        style={{flex: 1}}
        scalesPageToFit
        onContentProcessDidTerminate={syntheticEvent => {
          const { nativeEvent } = syntheticEvent;
          console.warn('Content process terminated, reloading', nativeEvent);
          this.setState({showWebView: false})
        }}
      />
    );
  }

  render() {
    const movie = this.props.navigation == null
      ? this.props.movie
      : this.props.navigation.getParam('movie');
    const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
    );
    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });

    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });

    const titleOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0.8, 1],
      extrapolate: 'clamp',
    });

    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 1, 0.8],
      extrapolate: 'clamp',
    });
    const titleTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, -8],
      extrapolate: 'clamp',
    });
  return (
    <View style={styles.fill}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="rgba(0, 0, 0, 0.251)"
        />
        <Animated.ScrollView
          style={styles.fill}
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true },
          )}
          contentInset={{
            top: HEADER_MAX_HEIGHT,
          }}
          contentOffset={{
            y: -HEADER_MAX_HEIGHT,
          }}
        >
          <TouchableOpacity
            onPress={() => this.setState({showWebView: true})}
            style={styles.trailerBtn}
          >
            <Text style={styles.trailerBtnText}>
              Трейлер
            </Text>
          </TouchableOpacity>

          <Genres />

          { this.state.showWebView && this.renderContent() }
          <Text style={styles.overview}>
            {movie.overview}
          </Text>
          <CastList movieId={movie.id} />
        </Animated.ScrollView>
        <Animated.View
          pointerEvents="none"
          style={[
            styles.header,
            { transform: [{ translateY: headerTranslate }] },
          ]}
        >
          <Animated.Image
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }],
              },
            ]}
            source={{ uri: `https://image.tmdb.org/t/p/w500${movie.poster_path}` }}
          />
        </Animated.View>
        <Animated.View
          style={[
            styles.bar,
            {
              opacity: titleOpacity,
              transform: [
                { scale: titleScale },
                { translateY: titleTranslate },
              ],
            },
          ]}
        >
          <Text style={styles.title}>{movie.title}</Text>
        </Animated.View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    backgroundColor: '#1a143b',
    padding: 10
  },
  content: {
    flex: 1
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#281e55',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 40 : 38,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  overview: {
    color: 'white',
    fontSize: 18,
    padding: 10,
    marginTop: 10,
    marginBottom: 20
  },
  scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 30,
    color: '#fff',
  },
  trailerBtn: {
    paddingVertical: 3,
    backgroundColor: '#f80f4d',
    textAlign: 'center',
    borderRadius: 5
  },
  trailerBtnText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 25
  }
});