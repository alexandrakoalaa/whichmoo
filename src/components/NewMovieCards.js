import React, {useState, useEffect} from 'react';
import Swiper from 'react-native-deck-swiper';
import {Modal, StatusBar, TouchableHighlight, StyleSheet, Text, View, Dimensions, Image, Animated, PanResponder } from 'react-native';

import { getMovies } from '../store/actions/movie';
import { useDispatch, useSelector } from 'react-redux';

import MovieDescription from './MovieDescription';

import  { Ionicons } from '@expo/vector-icons';

export const NewMovieCards = (props) => {
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    dispatch(getMovies());
  }, []);

  const allMovies = useSelector(state => state.movie.allMovies);
    return(
      <>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="rgba(0, 0, 0, 0.251)"
        />
        <Swiper
            cards={allMovies}
            renderCard={(card) => {
                return (
                  <View style={styles.card}>
                      <Image 
                        style={{flex: 1, height: null, width: null, resizeMode: 'cover', borderRadius: 20}} 
                        source={{uri: `https://image.tmdb.org/t/p/w500${(card || {}).poster_path}`}}
                      />
                  </View>
                )
            }}
            onSwipedAll={() => {console.log('onSwipedAll')}}
            onTapCard={(index) => {
              setCurrentIndex(index);
              setModalVisible(true);
            }}
            onSwipedRight={(index) => dispatch({type: 'SET_BOOKED_MOVIE', payload: allMovies[index]})}
            backgroundColor={'#1a143b'}
            verticalSwipe={false}
            stackSeparation={0}
            stackScale={10}
            cardIndex={0}
            stackSize= {2}
            cardStyle={{height: 650}}
        >
        </Swiper>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalVisible}
        >
            <Ionicons 
              name='ios-close-circle'
              size={26}
              color='#fff'
              style={{position: 'absolute', top: 45, right: 20, zIndex: 9999}}
              onPress={() => {
                setModalVisible(false);
              }}
            />
          <MovieDescription movie={allMovies[currentIndex]} />
        </Modal>
      </>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  card: {
    flex: 1,
    borderRadius: 25,
    justifyContent: "center",
    backgroundColor: "white"
  },
  text: {
    textAlign: "center",
    fontSize: 50,
    backgroundColor: "transparent"
  }
});