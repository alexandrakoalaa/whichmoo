export const LOAD_MOVIES = 'LOAD_MOVIES';
export const GET_MOVIES = 'GET_MOVIES';
export const TOGGLE_BOOKED = 'TOGGLE_BOOKED';
export const REMOVE_MOVIE = 'REMOVE_MOVIE';