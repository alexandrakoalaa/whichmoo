import { LOAD_MOVIES, GET_MOVIES, TOGGLE_BOOKED, REMOVE_MOVIE } from '../types';
import { randomPage } from '../../utils';
import { API_KEY } from '../../api_key';

export const getMovies = () => async (dispatch) => {
  const data = await fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&language=ru-RU&page=${randomPage()}`)
        const { results = [] } = await data.json();
        dispatch({type: GET_MOVIES, payload: results});
};

export const toggleBooked = id => {
  return {
    type: TOGGLE_BOOKED,
    payload: id
  }
};

export const removeMovie = id => {
  return {
    type: REMOVE_MOVIE,
    payload: id
  }
}