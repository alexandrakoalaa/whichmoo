import { GET_MOVIES, TOGGLE_BOOKED, REMOVE_MOVIE } from '../types';

const initialState = {
  allMovies: [],
  bookedMovies: []
}

export const movieReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MOVIES: return {
      ...state,
      allMovies: action.payload,
      bookedMovies: action.payload.filter(movie => {
        return movie.booked
      })
    }
    case 'SET_BOOKED_MOVIE':
      return {
        ...state,
        bookedMovies: [...state.bookedMovies, action.payload]
    }
    
    case REMOVE_MOVIE:
      return {
        ...state,
        bookedMovies: state.bookedMovies.filter(movie => movie.id !== action.payload)
      }

    default: return state
  }
}