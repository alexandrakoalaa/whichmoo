import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('movie.db');

export class DB {
  static init() {
    return new Promise((resolve, reject) => {
      db.transaction(tx => {
        tx.executeSql(
          'CREATE TABLE IF NOT EXISTS movies (id INTEGER PRIMARY KEY NOT NULL, title TEXT NOT NULL, image TEXT NOT NULL, year INTEGER NOT NULL)',
          [],
          resolve,
          (_, error) => reject(error)
        )
      })
    });
  }
}