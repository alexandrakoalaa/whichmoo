import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import  { Ionicons } from '@expo/vector-icons';

import {NewMovieCards} from '../components/NewMovieCards';
import {FavouriteMoviesList} from '../components/FavouriteMoviesList';
import MovieDescription from '../components/MovieDescription';

const FavouriteMoviesStack = createStackNavigator({
  FavouriteList: {
    screen: FavouriteMoviesList,
    navigationOptions: {
      title: 'Список фильмов',
      headerStyle: {
        backgroundColor: '#281e55',
        shadowColor: 'transparent'
      },
      headerTitleStyle: {
        color: '#fff'
      }
    },
  },
  
  FavouriteMovie: {
    screen: MovieDescription,
    navigationOptions: {
      title: 'Список фильмов',
      headerShown: false,
      headerStyle: {
        backgroundColor: '#281e55',
        shadowColor: 'transparent'
      },
      headerTitleStyle: {
        color: '#fff'
      }
    }
  },
});

const BottomNavigation = createBottomTabNavigator({
  Cards: {
    screen: NewMovieCards,
    navigationOptions: {
      tabBarIcon: info => {
        return <Ionicons name='ios-browsers' size={22} color={info.tintColor} />
      }
    } 
  },
  List: {
    screen: FavouriteMoviesStack,
    navigationOptions: {
      tabBarIcon: info => {
        return <Ionicons name='ios-list-box' size={22} color={info.tintColor} />
      }
    } 
  }
},{
  tabBarOptions: {
    activeTintColor: '#f80f4d',
    style: {
      backgroundColor: '#281e55',
    },
  }
})

export const AppNavigation = createAppContainer(BottomNavigation);